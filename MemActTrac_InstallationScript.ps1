﻿<#
    This installation script provided by First Strike Development
    installs the MemActTrac application.  Run this script on the 
    AWS server you have provisioned.  This script assumes that 
    you have provisioned a Windows 2012 server, and have downloaded
    SQL Server onto the Windows Server.

    This script will install Git, Ruby, Ruby on Rails, 
    and the MemActTrac repository.  This script will also open the 
    TCP ports for SQL server so that the application can be set up
    and run properly.  
    
    After opening the proper ports on SQL Server, this script will 
    create the MemActTrac database, migrate the database schema, populate 
    the database with seed values, and finally import the client's
    historic data.
#>

echo "Hello! We are about to install the Membership Activity Tracker by First Strike Development"
echo "Please stay at the computer as this installation script runs, since you will need to respond to prompts"

echo "First, we're going to download and install ruby"

.\rubyinstaller-devkit-2.4.4-2-x64.exe
$Env:Path = $Env:Path + "C:\Ruby24-x64\bin;"

echo "Now, the installer will install Rails"
C:\Ruby24-x64\bin\gem install rails --version 5.1.6

echo "Great! Next up, we're going to install Git so we can download the application from the internet"
.\Git-2.16.2-64-bit.exe
$Env:Path = $Env:Path + "C:\Program Files\Git\cmd\git.exe"

echo "This is going well, we're almost there"

echo "Next, we're going to make sure the SQL Server Ports are configured right"
echo "This step will fail if you have not yet downloaded SQL Server"

Import-Module "sqlps"
$smo = 'Microsoft.SqlServer.Management.Smo'
$wmi = new-object ($smo + 'Wmi.ManagedComputer')
$uri = "ManagedComputer[@Name='COT-CIS4375-02']/ ServerInstance[@Name='cis4375']/ServerProtocol[@Name='Tcp']"
$Tcp = $wmi.GetSmoObject($uri)
$Tcp.IsEnabled = $true
$Tcp.Alter()
$uri = "ManagedComputer[@Name='COT-CIS4375-02']/ ServerInstance[@Name='cis4375']/ServerProtocol[@Name='Np']"
$Np = $wmi.GetSmoObject($wmi)
$Np.IsEnabled = $true
$Np.Alter()

echo "We will now download the Membership Activity Tracker from Git"
git clone https://gitlab.com/erikwhiting/MemActTrac.git

echo "We're really close now"
echo "Next, we're going to build the database and import all the old data"

cd MemActTrac.git
C:\Ruby24-x64\bin\rails db:create
C:\Ruby24-x64\bin\rails db:migrate
C:\Ruby24-x64\bin\rails db:seed
cd ..
Invoke-Sqlcmd -MemActTrac MemActTrac -InputFile .\import_legacy_data.sql

echo "The last thing we will do is start up the server"
echo "Copy and paste the DNS of your AWS server instance and appended :3000"
echo "or if you're working from the server, go to 'https://localhost:3000'"
echo "Have a great day!"
C:\Ruby24-x64\bin\rails server